#include <stdio.h>
#include <math.h>	 /* needed for pow() function */
#include <limits.h>  /* limits for integers */


long maxlong(void)
{
	return LONG_MAX;			/* Returns the highest possible long */
}


double upper_bound(long n)
{
	double f;
	
	if (0 <= n && n < 6){
		f = 719;
	}
	else{
		f = pow(n/2.0, n);			/* Division includes float to avoid truncation in integer division */
	}
	return f;
}


long factorial(long l)		/* Calculates factorials and outputs a long integer if possible */
{
	long L = 1;
	int i;
	if (l >= 0){						/* Only calculates on a positive input */
		for(i=1; i<=l; i++){
			L *= i;
		}
		if (maxlong() <= upper_bound(l)){ /* Test whether the upper bound would go over the long limit */
			L = -1;					/* upper bound is too high */
		}
	}
	else {
		L = -2;						/* input is negative */
	}
	return L;
}


int main(void) {
    long i;
	long L = 11;

    /* The next line should compile once "maxlong" is defined. */
    printf("maxlong()=%ld\n", maxlong());

    /* The next code block should compile once "upper_bound" is defined. */

    for (i=0; i<20; i++) {
        printf("upper_bound(%ld)=%g\n", i, upper_bound(i));
    }
    
	/* The next block shows -2 for negative inputs, -1 for outputs greater than allowable */
	printf("factorial(%ld)=%ld\n", L, factorial(L));
	
	
    return 0;
	
}


